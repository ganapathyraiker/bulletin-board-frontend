import Vue from 'vue'
import router from './router'
import App from './App.vue'
import vuetify from 'plugins/vuetify'

import 'assets/style/app.scss'

/* eslint-disable-next-line no-new */
new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')