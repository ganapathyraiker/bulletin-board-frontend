import { mount } from '@vue/test-utils'
import HelloComponent from '../src/components/HelloComponent.vue'

test('Component has class global-container', () => {

  const wrapper = mount(HelloComponent, {
    propsData: {
      name: "lorem ipsum"
    }
  })

  expect(wrapper.classes()).toContain('component-container')
})